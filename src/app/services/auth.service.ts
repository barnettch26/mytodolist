import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';

import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient, private storage: Storage, private router: Router) { }

  signIn(em, pw){

    this.http.post('http://todolist.local/api/login', {
      email: em,
      password: pw
    }).subscribe((response) => {
      //this.storage.set('ACCESS_TOKEN', response['access_token']);
      console.log(response);

      this.setToken(response).then((val) => {
        if(response['success']==true){
          this.setLoggedIn('true').then((val) => {
            this.getFromStorage();
          });
        }
        else{
          this.setLoggedIn('false').then((val) => {
            this.getFromStorage();
          })
        }
      });

      /*if(response['success']==true){
        this.storage.set('LOGGED_IN', 'true');
      }
      else{
         this.storage.set('LOGGED_IN', 'false');
      }*/

      /*this.storage.get('ACCESS_TOKEN').then((val) => {
          console.log('Your access token is ', val);

          this.storage.get('LOGGED_IN').then((val) => {
              console.log('Your log in state is ', val);
              if(val == 'true'){
                this.router.navigateByUrl('/todolist');
              }
          });
      });*/

    },onerror=>{console.log('ERROR MESSAGE');}
    );

    /*this.storage.get('LOGGED_IN').then((val) => {
        if(val == 'true'){
          this.router.navigateByUrl('/todolist');
        }
    });*/
  }

  logOut(){
    this.storage.set('ACCESS_TOKEN', '').then((val) => {
      this.storage.set('LOGGED_IN', 'false').then((val) => {
        this.router.navigateByUrl('/login');
      });
    });
  }

  setToken(response){
    return this.storage.set('ACCESS_TOKEN', response['access_token']);
  }

  setLoggedIn(loggedIn){
    return this.storage.set('LOGGED_IN', loggedIn);
  }

  getFromStorage(){
    return this.storage.get('ACCESS_TOKEN').then((val) => {
        console.log('Your access token is ', val);

        this.storage.get('LOGGED_IN').then((val) => {
            console.log('Your log in state is ', val);
            if(val == 'true'){
              this.router.navigateByUrl('/todolist');
            }
        });
    });
  }

}
