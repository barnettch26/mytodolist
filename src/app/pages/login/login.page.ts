import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  email = '';
  password = '';

  constructor(private http: HttpClient, private auth: AuthService, private router: Router, private storage: Storage) {

  }

  ngOnInit() {

  }

  signIn(){
    this.auth.signIn(this.email, this.password);
  }
}
