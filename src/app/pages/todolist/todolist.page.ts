import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Component({
  selector: 'app-todolist',
  templateUrl: './todolist.page.html',
  styleUrls: ['./todolist.page.scss'],
})
export class TodolistPage implements OnInit {
  tasksArr = [];
  headers: HttpHeaders;

  constructor(private auth: AuthService, private storage: Storage, private router: Router, private http: HttpClient) {

  }

  ngOnInit() {

  }

  ionViewWillEnter(){
    this.checkLoggedIn().then ((val) => {
      this.setAccessToken().then ((val) => {
      });
    });

    this.tasksArr = [];
    this.http.get('http://todolist.local/api/profiles/tasks', {headers: this.headers}).subscribe((response) => {

      for(var i = 0; i < response['tasks'].length; i++){
        this.tasksArr.push(response['tasks'][i]);
      };
    });
  }


  logOut() {
    this.auth.logOut();
  }

  checkLoggedIn(){
    return this.storage.get('LOGGED_IN').then((val) => {
      if(val == 'false'){
        this.router.navigateByUrl('/login');
      }
    });
  }

  setAccessToken(){
    return this.storage.get('ACCESS_TOKEN').then((val) => {
        console.log('2. Access Token: ', val);
        this.setHeaders(val);
    });
  }

  setHeaders(token){
    var bearer = 'Bearer '+token;
    this.headers= new HttpHeaders(
       {
         'Accept': 'application/json',
         'Authorization': bearer,
       }
     );
  }

}
